let getCube = 2;
console.log(`The cube of ${getCube} is ${getCube ** 3}`);

let address = [258, "Washington Ave NW", "California", 90011];

let [streetNumber, city, country, postalCode] = address;
console.log(`I live at ${streetNumber} ${city}, ${country} ${postalCode}`);

let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	kgWeight: "1075",
	length: "20 ft 3 in"
};

let {name, type, kgWeight, length} = animal;
console.log(`${name} was a ${type}. He weighed at ${kgWeight} kgs with a measurement of ${length}.`);

let numberArray = [1, 2, 3, 4, 5];

const printNumber =

numberArray.forEach((number) => {console.log(number);});

class Dog {
	constructor(name, age, breed) {
		this.name = name,
		this.age = age,
		this.breed = breed
	};
};

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);